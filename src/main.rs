extern crate crossterm;
extern crate savefile;

mod threadpool;

use rand::prelude::*;
use std::sync::{Arc, Mutex};
use threadpool::ThreadPool;
use std::io;
use std::io::prelude::*;
use std::fs::OpenOptions;
use crossterm::event::{read, Event, KeyCode, KeyEvent, KeyModifiers};
use std::{thread};
use std::time::{Instant, Duration};
use savefile::prelude::*;
use std::path::Path;


#[macro_use]
extern crate savefile_derive;

#[derive(Savefile)]
struct Data {
	save_iteration : i64,
	save_blaze : i32,
	save_pearl : i32,
	save_best_blaze : i32,
	save_best_pearl : i32,
	save_score : i32,
}

fn save_data(data:&Data) {
	save_file("save.bin", 0, data).unwrap();
}

fn load_data() -> Data {
	load_file("save.bin", 0).unwrap()
}

fn main() -> Result<(), rand::Error> {
	if Path::new("./save.bin").exists() == false {  //Checks if savefile exists.  If false, create new savefile.
		save_data(&Data { save_iteration: 0, save_blaze: 0, save_pearl: 0, save_best_blaze: 0, save_best_pearl: 0, save_score: 0 });
	}
	let import = load_data();
	
    let n_workers = num_cpus::get() - 1;  //Using less than max threads can prevent system hangs with high background usage, very little performance impact.
	println!("Using {} CPU threads...", n_workers);
    //let n_jobs = 100000;                             //for finite runs
    let mut pool = ThreadPool::new(n_workers);
    
    let max_blaze = Arc::new(Mutex::new(import.save_blaze));
    let max_pearl = Arc::new(Mutex::new(import.save_pearl));
    let best_run_blaze = Arc::new(Mutex::new(import.save_best_blaze));
    let best_run_pearl = Arc::new(Mutex::new(import.save_best_pearl));
    let max_score = Arc::new(Mutex::new(import.save_score));
    let mut total_iteration = import.save_iteration;
    let stop_arc = Arc::new(Mutex::new(false));
    let reset_arc = Arc::new(Mutex::new(false));
    
    let mut start_time = Instant::now();              //for elapsed time and clock formatting
    //let mut elapsed_time = Duration::new(0, 0);
    let mut time_i = 0;
    let mut elapsed_days = 0i64;
    let mut elapsed_hours = 0i64;
    let mut elapsed_mins = 0i64;
    let mut elapsed_secs = 0i64;
    
    let mut log_i = 0;
    let mut log = OpenOptions::new().append(true).create(true).open("log.txt").expect("cannot open file");
    
    
    println!();
    println!("'CTRL + q' - Save and Exit Program");
    println!("'CTRL + r' - Reset Data (and Save File)");
    println!();
	println!("{:20}{:20}{:30}{:30}", "Elapsed Time:", "Iteration:", "Best Combined Run:", "Best Individual:");
	println!("{:>40}{:10}{:20}{:10}{:20}", "T  B  M  t     ", "Rods", "Pearls", "Rods", "Pearls");
	print!("\r{:>38}  {:<10}{:<20}{:<10}{:<20}", total_iteration, *best_run_blaze.lock().unwrap(), *best_run_pearl.lock().unwrap(), *max_blaze.lock().unwrap(), *max_pearl.lock().unwrap());
			
	let stop_loop = stop_arc.clone();
	let data_reset = reset_arc.clone();
	thread::spawn(move || {                             //This thread is spawned to detect input keypresses while main loop is executing
		let _no_modifiers = KeyModifiers::empty();      
		'key: loop {
			match read().unwrap() {
				Event::Key(KeyEvent {
					code: KeyCode::Char('q'),
					modifiers: KeyModifiers::CONTROL,
				}) => {
					let mut stop = stop_loop.lock().unwrap();
					*stop = true;
					break 'key;
					}
				Event::Key(KeyEvent {
					code: KeyCode::Char('r'),
					modifiers: KeyModifiers::CONTROL,
				}) => {
					let mut reset = data_reset.lock().unwrap();
					*reset = true;
				}	
				_ => (),
			}
		}
	});
			
			//for _ in 0..n_jobs {                  //for finite runs
			'main: loop {
				
				let data_blaze = max_blaze.clone();
				let data_pearl = max_pearl.clone();
				let data_best_blaze = best_run_blaze.clone();
				let data_best_pearl = best_run_pearl.clone();
				let data_score = max_score.clone();
				

				pool.execute(move |rng| {
					let mut local_blaze_max = 0;
					let mut local_pearl_max = 0;
					let mut local_score = 0f32;
					let mut local_best_blaze = 0;
					let mut local_best_pearl = 0; 
					for _ in 0..10000 {
						let mut total_blaze = 0;
						for _ in 0..305 {
							if rng.gen_bool(1.0/2.0) {
								total_blaze += 1;
							}
						}
						if total_blaze > local_blaze_max {
							local_blaze_max = total_blaze;
						}
						let mut total_pearl = 0;
						for _ in 0..262 {
							if rng.gen_bool(20.0/423.0){
								total_pearl += 1;
							}
						}
						if total_pearl > local_pearl_max {
							local_pearl_max = total_pearl;
						}
						
						let score = total_blaze as f32 + total_pearl as f32 * 4.316f32; // score to evaluate the "best" cummulative run, pearls are weighted based on (211/305)/(42/262) (probably not a good way to do this).
						if score > local_score {
							local_score = score;
							local_best_blaze = total_blaze;
							local_best_pearl = total_pearl;
						}
					}

					let mut current_blaze_max = data_blaze.lock().unwrap();
					let mut current_pearl_max = data_pearl.lock().unwrap();
					let mut current_best_blaze = data_best_blaze.lock().unwrap();
					let mut current_best_pearl = data_best_pearl.lock().unwrap();
					let mut current_score = data_score.lock().unwrap();
					
					if local_score > *current_score as f32 {
						*current_score = local_score as i32;
						*current_best_blaze = local_best_blaze;
						*current_best_pearl = local_best_pearl;
						print!("\r{:>38}  {:<10}{:<20}{:<10}{:<20}", total_iteration, *current_best_blaze, *current_best_pearl, *current_blaze_max, *current_pearl_max);
						io::stdout().flush().unwrap();
					}
				   
					if local_blaze_max > *current_blaze_max {
						*current_blaze_max = local_blaze_max;
						print!("\r{:>38}  {:<10}{:<20}{:<10}{:<20}", total_iteration, *current_best_blaze, *current_best_pearl, *current_blaze_max, *current_pearl_max);
						io::stdout().flush().unwrap();
					}
					if local_pearl_max > *current_pearl_max {
						*current_pearl_max = local_pearl_max;
						print!("\r{:>38}  {:<10}{:<20}{:<10}{:<20}", total_iteration, *current_best_blaze, *current_best_pearl, *current_blaze_max, *current_pearl_max);
						io::stdout().flush().unwrap();
					}	
				});
			
				total_iteration += 10000;  //rough iteration counter incrementing on each completed job
				time_i += 1;
				if time_i == 100 {
					let elapsed_time = start_time.elapsed();
					elapsed_days = elapsed_time.as_secs() as i64 / 86400;
					elapsed_hours = elapsed_time.as_secs() as i64 / 3600 - elapsed_days as i64 * 24;
					elapsed_mins = elapsed_time.as_secs() as i64 / 60 - (elapsed_hours as i64 * 60 + elapsed_days as i64 * 1440);
					elapsed_secs = elapsed_time.as_secs() as i64 - (elapsed_mins as i64 * 60 + elapsed_hours as i64 * 3600 + elapsed_days as i64 * 86400);
					time_i = 0;
				}
				log_i += 1;
				if log_i == 500000 {  //write a log entry and save progress every 5 billion iterations
					log_i = 0;
					let output = format!("\nElapsed time: {}d{:2}h{:2}m{:2}s  Iteration: {}  Best Combined Run:({} Rods, {} Pearls)  Individual Max:({} Rods, {} Pearls)", elapsed_days as i64, elapsed_hours as i8, elapsed_mins as i8, elapsed_secs, total_iteration, *best_run_blaze.lock().unwrap(), *best_run_pearl.lock().unwrap(), *max_blaze.lock().unwrap(), *max_pearl.lock().unwrap());
					log.write_all(output.as_bytes()).expect("write failed");
					save_data(&Data { save_iteration: total_iteration, save_blaze: *max_blaze.lock().unwrap(), save_pearl: *max_pearl.lock().unwrap(), save_best_blaze: *best_run_blaze.lock().unwrap(), save_best_pearl: *best_run_pearl.lock().unwrap(), save_score: *max_score.lock().unwrap() });
				}
					
			
				print!("\r{:8}d{:2}h{:2}m{:2}s{:>20} ", elapsed_days as i64, elapsed_hours as i8, elapsed_mins as i8, elapsed_secs, total_iteration);
				io::stdout().flush().unwrap();
				
				if *stop_arc.lock().unwrap() {
					break 'main;
				}
				if 	*reset_arc.lock().unwrap() {
					*reset_arc.lock().unwrap() = false;
					*max_blaze.lock().unwrap() = 0;
					*max_pearl.lock().unwrap() = 0;
					*best_run_blaze.lock().unwrap() = 0;
					*best_run_pearl.lock().unwrap() = 0;
					*max_score.lock().unwrap() = 0;
					total_iteration = 0;
					save_data(&Data { save_iteration: total_iteration, save_blaze: *max_blaze.lock().unwrap(), save_pearl: *max_pearl.lock().unwrap(), save_best_blaze: *best_run_blaze.lock().unwrap(), save_best_pearl: *best_run_pearl.lock().unwrap(), save_score: *max_score.lock().unwrap() });
					print!("\r{:60}", "Data reset. Resuming from zero...");
					io::stdout().flush().unwrap();
					log.write_all("\nData reset by user".as_bytes()).expect("write failed");
					thread::sleep(Duration::from_secs(2));
					start_time = Instant::now();	
				}				
			}
			
	save_data(&Data { save_iteration: total_iteration, save_blaze: *max_blaze.lock().unwrap(), save_pearl: *max_pearl.lock().unwrap(), save_best_blaze: *best_run_blaze.lock().unwrap(), save_best_pearl: *best_run_pearl.lock().unwrap(), save_score: *max_score.lock().unwrap() });	
    drop(pool); 
    let result_blaze = max_blaze.lock().unwrap();
    let result_pearl = max_pearl.lock().unwrap();
    let result_best_blaze = best_run_blaze.lock().unwrap();
    let result_best_pearl = best_run_pearl.lock().unwrap();
	println!();
    println!("Result! Best combined run: {} Blaze Rods, {} Pearls. (Best individual: {} Blaze Rods, {} Pearls.)", result_best_blaze, result_best_pearl, result_blaze, result_pearl);
    println!("Data saved, restart program to resume");
    println!("Exiting in 5s...");
    thread::sleep(Duration::from_secs(5));
    Ok(())
   
}


	

			
