# barter-simulation
Multithreaded CPU simulation for piglin barters and blaze kills in Dream's 6 contested streams. Very high CPU usage, use with discretion.

## How to Use

### Windows
Download the Windows package from [releases](https://gitlab.com/77xak/barter-simulation/-/releases).  
Extract and run barter-simulation.exe.

### Linux or to Compile from Source Code
Requirements:  
[Git](https://git-scm.com/download/)  
[Rust](https://www.rust-lang.org/tools/install)

Open terminal (CMD or Powershell in Windows).  
`git clone` repository into desired directory.  
`cd barter-simulation`  
Run with `cargo run --release`. (`--release` flag required for optimized performance.)    

## 1.1.0
- Released pre-compiled .exe for Windows with no dependencies (hopefully).
- Added savefile. On restart, resume from last save instead of starting from 0. (Can be reset with `Ctrl + r`.)
- Added logfile. Logs new entry every 5 billion iterations.
- Added elapsed time indicator.
- Minor performance improvement (~3%) by providing threads with larger jobs.

## 1.0.1  
- Can now gracefully exit with `Ctrl + q`

## 1.0.0
- Added blaze rod simulation
- Added "best" combined run tracking (highest number of pearls and rods obtained in a single stream iteration)
- Added iteration counter
- Reduced system hang from high CPU usage
- *Very* basic text UI

## 0.1.0
I made this to emulate piglin barters with the attempt of replicating Dream's luck,
with regards to only ender pearls.

This is a multithreaded simulation, so it should run much faster than other simulations
out there--on my machine it does over 100 billion RNG calls per minute. The tradeoff is
that this will eat up a lot of CPU power.

The secret sauce here is a custom threadpool that gives each worker thread its own source
of RNG, so you don't need to re-seed RNG every new job or share RNG between threads
causing excessive locking.

